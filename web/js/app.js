
function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};

async function loadCertificate(serverCertificatePath, cb) 
        {
        	if(serverCertificatePath) {
	            var request = new XMLHttpRequest();
	            request.addEventListener('load', onCertificateLoaded, false);
	            request.addEventListener('error', onCertificateError, false);
	            request.open('GET', serverCertificatePath, true);
	            request.setRequestHeader('Pragma', 'Cache-Control: no-cache');
	            request.setRequestHeader("Cache-Control", "max-age=0");
				request.send();
				request.onreadystatechange = function() {
					if(request.readyState === 4)
					{
					  if(request.status === 200)
					  {
						cb( new Uint8Array(shaka.util.Uint8ArrayUtils.fromBase64(request.response).buffer));
					  }
					}
				  };
			} else 
				cb(null);
        }


		function onCertificateLoaded(event, cb) {
            var request = event.target;
            certificate = new Uint8Array(request.response);
        }

        function onCertificateError(event) {
            window.console.error('Failed to retrieve the server certificate.')
        }

async function initApp() {
	document.getElementById("manifest-url").value = getUrlParameter('manifestUrl');
	document.getElementById("license-url").value = getUrlParameter('licenseUrl');
	document.getElementById("certificate").value = getUrlParameter('certificate');
	document.getElementById("kid").value = getUrlParameter('kid');


  shaka.polyfill.installAll();
  if (shaka.Player.isBrowserSupported()) {
	  var license = getUrlParameter('licenseUrl');
	  // var license = document.getElementById('license-url').value;
	  var WIDEVINE = false;
	  var FAIRPLAY = false;
	  var support = await shaka.Player.probeSupport();
	  console.log("support", support.drm);

	  if(support['drm']['com.widevine.alpha'] && support['drm']['com.widevine.alpha'] != null) {
		WIDEVINE = true;
	  }
	  if((support['drm']['com.apple.fps'] && support['drm']['com.apple.fps'] != null )|| (support['drm']['com.apple.fps.1_0'] && support['drm']['com.apple.fps.1_0'] != null)) {
		FAIRPLAY = true;
	  }
	  
	  if(WIDEVINE) {
		 license = getUrlParameter('licenseUrl');
		 var manifests = [
			{title: "BigBuckBunny", stream : getUrlParameter('manifestUrl')}
		];
		var manifest = manifests[0].stream;
		if(manifest) {
			initPlayer('video1', manifest, license, "");
		} else {
			console.log("manifestUrl misssing");
		}
	  }

	  if(FAIRPLAY) {
		  // license = 'https://localhost:9443/license/fairplay';
		  license = getUrlParameter('licenseUrl');
		  // license = document.getElementById('license-url').value;
		  var manifests = [
			// {title: "KingsM", stream : "http://localhost:8080/output/manifest.m3u8"}
			{title: "KingsM", stream : getUrlParameter('manifestUrl')}
			// {title: "KingsM", stream : document.getElementById('manifest-url').value}
			
		];
		var kid = getUrlParameter('kid');
		var manifest = manifests[0].stream;
		console.log("drm",FAIRPLAY, license , manifest)
		console.log("KID", kid);
		console.log(manifest, license);
		// await loadCertificate('http://localhost:8080/fp/fairplay-sony.cer', function(cert){
			await loadCertificate(getUrlParameter('certificate'), function(cert){
				// await loadCertificate(document.getElementById('certificate').value, function(cert){
			// await loadCertificate('https://localhost:9443/license/fairplay/certificate', function(cert){
			if(manifest) {
				console.log("drm",FAIRPLAY)
				initPlayerFP('video1', manifest, license, cert, kid);
			} else {
				console.log("manifestUrl misssing");
			}
	    });
	  }
	  
  } else {
    console.error('Browser not supported!');
  }
}

function loadVideo(){
	console.log("Load Video");
	var resouceurl = document.getElementById("manifest-url").value;
	var licenseurl = document.getElementById("license-url").value;
	var certificate = document.getElementById("certificate").value;
	var kid = document.getElementById("kid").value;
	var url = window.location.origin + window.location.pathname + "?manifestUrl="+resouceurl + "&licenseUrl="+licenseurl +"&certificate="+certificate +"&kid="+kid;
	if(!resouceurl) {
		alert("Please Enter Manifest url")
	} else {
		  window.location = url;
	}
}

function initPlayer(ele, manifestUri, licenseServer, certificate) {

  if(window.player){
	  window.player.detach();
  }
  
  console.log("manifestUri", manifestUri);
	console.log("licenseServer", licenseServer);
  
	var video = document.getElementById(ele);
	var container = document.getElementById('videouitag');
  
	var player = new shaka.Player(video);
	
	let ui = new shaka.ui.Overlay(player, container, video);
	const uiConfig={
		'addSeekBar': true,
    'addBigPlayButton':false,
		'controlPanelElements':['play_pause','time_and_duration','spacer','mute','volume', 'fullscreen', 'overflow_menu'],
		'overflowMenuButtons' : ['captions','language','picture_in_picture']
  }
	let controls = ui.getControls();
	ui.configure(uiConfig);
  controls.setEnabledShakaControls(true);
  controls.setEnabledNativeControls(false);
  
  var Uint8ArrayUtils = shaka.util.Uint8ArrayUtils;
    
  var advanceConfig = {
	 persistentStateRequired: true
  }

  player.configure({
	  drm: {
	    servers: {
	      'com.widevine.alpha': licenseServer
	    },
	    advanced: {
	      'com.widevine.alpha': advanceConfig 
	    }
	  }
   });
   
	window.player = player;
	window.ui = ui;
  player.addEventListener('error', onErrorEvent);

  player.load(manifestUri).then(function() {
		console.log('The video has now been loaded!');
		video.play();
  }).catch(onError);

}

function initPlayerFP(ele, manifestUri, licenseServer, certificate, kid) {
	console.log("KID", kid);
	
	if(window.player){
		window.player.detach();
	}
	
	console.log("manifestUri", manifestUri);
	console.log("licenseServer", licenseServer);

	var video = document.getElementById(ele);
	var container = document.getElementById('videouitag');
	
	var player = new shaka.Player(video);

	let ui = new shaka.ui.Overlay(player, container, video);
	const uiConfig={
		'addSeekBar': true,
    'addBigPlayButton':false,
		'controlPanelElements':['play_pause','time_and_duration','spacer','mute','volume', 'fullscreen', 'overflow_menu'],
		'overflowMenuButtons' : ['captions','language','picture_in_picture']
  }
	let controls = ui.getControls();
	ui.configure(uiConfig);
  controls.setEnabledShakaControls(true);
  controls.setEnabledNativeControls(false);
	
	var advanceConfig = {};	  
	if(certificate) 
		advanceConfig.serverCertificate = certificate
	
	console.log("advanceConfig",certificate);
	player.configure({
		drm: {
		  servers: {
			'com.apple.fps.1_0': licenseServer
		  },
		  advanced: {
			'com.apple.fps.1_0': advanceConfig 
		  },
		//   initDataTransform: (initData) => {
		// 	const contentId = 'localhost';
		// 	const cert = player.drmInfo().serverCertificate;
		// 	return shaka.util.FairPlayUtils.initDataTransform(initData, contentId, cert);
		//   }
		}
		
	 });

	//player.configure('drm.advanced.com\\.apple\\.fps\\.1_0.serverCertificate', new Uint8Array(certificate));
	
	player.getNetworkingEngine().registerRequestFilter((type, request) => {
		if (type != shaka.net.NetworkingEngine.RequestType.LICENSE) {
		  return;
		}
	  
		const originalPayload = new Uint8Array(request.body); console.log(2, originalPayload);
		const base64Payload =
			shaka.util.Uint8ArrayUtils.toStandardBase64(originalPayload); console.log(3, base64Payload);
		const params = 'spc=' + encodeURIComponent(base64Payload) + '&assetId='+encodeURIComponent(kid);
		request.headers['Content-Type'] = 'application/x-www-form-urlencoded';
		request.body = params;
	  });
	  
	  player.getNetworkingEngine().registerResponseFilter((type, response) => {
		if (type != shaka.net.NetworkingEngine.RequestType.LICENSE) {
		  return;
		}
	  
		let responseText = shaka.util.StringUtils.fromUTF8(response.data);
		// Trim whitespace.
		responseText = responseText.trim();
	  
		// Look for <ckc> wrapper and remove it.
		if (responseText.substr(0, 5) === '<ckc>' &&
			responseText.substr(-6) === '</ckc>') {
		  responseText = responseText.slice(5, -6);
		}
	  
		// Decode the base64-encoded data into the format the browser expects.
		response.data = shaka.util.Uint8ArrayUtils.fromBase64(responseText).buffer;
	  });

	window.player = player;
	window.ui = ui;
	player.addEventListener('error', onErrorEvent);
  
	player.load(manifestUri).then(function() {
		console.log('The video has now been loaded!');
		video.play();
	}).catch(onError);
  
  }

function onErrorEvent(event) {
  onError(event.detail);
}

function onError(error) {
  console.error('Error code', error.code, 'object', error);
}

document.addEventListener('DOMContentLoaded', initApp);
