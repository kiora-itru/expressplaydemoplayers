# ITRU-Demo Shaka Player

This demo player lets you play both encrypted and un-encrypted contents. Supported DRM for encrypted contents:

  - Widevine
  - Fairplay   

#### Installation

  - Download the entire repo onto the root directory of the web-server and unzip. (In case of Tomcat, ${TOMCAT_HOME}/webapps is the root directory.)
  - Configure the webserver to run on HTTPS. (For tomcat refer [here][InSSL])
  - Restart the web-server.
  - Open a browser and navigate to <https://localhost:8443/itrudemoshakaplayer/index.html>    


#### Widevine Playback (CMAF/DASH)

| Fields | Values |
| ------ | ------ |
| License Server URL | https://{LICENSE_SERVER_IP}:{LICENSE_SERVER_PORT}/edos/v1-beta/license/widevine |
| Stream URL | https://{STREAM_SERVER_IP}:{STREAM_SERVER_PORT}/.../..mpd |
| Certificate URL | https://{LICENSE_SERVER_IP}:{LICENSE_SERVER_PORT}/edos/v1-beta/license/widevine/certificate |    

#### Fairplay Playback (CMAF/HLS)

| Fields | Values |
| ------ | ------ |
| License Server URL | https://{LICENSE_SERVER_IP}:{LICENSE_SERVER_PORT}/edos/v1-beta/license/fairplay |
| Stream URL | https://{STREAM_SERVER_IP}:{STREAM_SERVER_PORT}/.../..m3u8 |
| Certificate URL | https://{LICENSE_SERVER_IP}:{LICENSE_SERVER_PORT}/edos/v1-beta/license/fairplay/certificate |
|KID|KID used during encryption of the CMAF/HLS file|    

#### Clear content Playback (CMAF/HLS)

| Fields | Values |
| ------ | ------ |
| Stream URL | https://{STREAM_SERVER_IP}:{STREAM_SERVER_PORT}/.../..(mpd/m3u8) |    

##### Disabling CORS header
- Google Chrome: Install [Allow CORS][DisCORS] plugin to disable CORS and enable the plugin.
- Safari: Enable the developer menu from Preferences >> Advanced, and select "Disable Cross-Origin Restrictions" from the develop menu.    

###### For more details, please send an email to: <kiora-support@intertrust.com>

   [InSSL]: <https://tomcat.apache.org/tomcat-7.0-doc/ssl-howto.html>
   [DisCORS]: <https://chrome.google.com/webstore/detail/allow-cors-access-control/lhobafahddgcelffkeicbaginigeejlf?hl=en>
  
