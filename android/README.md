# README

This demo player lets you play both encrypted and un-encrypted contents. Supported DRM for encrypted contents:

  - Widevine

#### Prerequisites

  - Please change boolean value of buildConfigField named IS_SMART_TV in build.gradle to switch between Smart TV and mobile devices.
  
  - buildConfigField 'boolean', 'IS_SMART_TV', 'Boolean.parseBoolean("true")' -> To play content on TV.
  - buildConfigField 'boolean', 'IS_SMART_TV', 'Boolean.parseBoolean("false")' -> To play content on Mobile devices.
  - By default it will support both Multi Audio and Subtitles


For playback on TV we've to provide dashUrl and licenseUrl strings (should hardcode in MainActivity.kt file)  



#### Widevine Playback (CMAF/DASH)

| Fields | Values |
| ------ | ------ |
| License Server URL | https://domain-name/edos/v1-beta/license/widevine |
| Stream URL | https://domain-name/.../..mpd |
   

#### Clear content Playback

| Fields | Values |
| ------ | ------ |
| Stream URL | https://domain-name/.../..(mpd/m3u8) |   

###### For more details, please send an email to: <kiora-support@intertrust.com>
  