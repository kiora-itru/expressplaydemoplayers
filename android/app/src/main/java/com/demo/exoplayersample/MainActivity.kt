package com.demo.exoplayersample

import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.drm.DefaultDrmSessionManager
import com.google.android.exoplayer2.drm.ExoMediaCrypto
import com.google.android.exoplayer2.drm.HttpMediaDrmCallback
import com.google.android.exoplayer2.source.dash.DashMediaSource
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.MappingTrackSelector
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.ui.TrackSelectionDialogBuilder
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.google.android.exoplayer2.util.Assertions
import com.google.android.material.textfield.TextInputEditText
import java.security.SecureRandom
import java.security.cert.X509Certificate
import javax.net.ssl.*


class MainActivity : AppCompatActivity(), View.OnClickListener {

    companion object {
        private const val USER_AGENT = "Exo-Player"
    }

    private lateinit var bandwidthMeter: DefaultBandwidthMeter
    private lateinit var drmSessionManager: DefaultDrmSessionManager<ExoMediaCrypto>
    private lateinit var selector: DefaultTrackSelector
    private val loadControl = DefaultLoadControl()
    private lateinit var player: SimpleExoPlayer
    private lateinit var urlEditText: TextInputEditText
    private lateinit var licenseUrlEditText: TextInputEditText
    private lateinit var playButton: Button
    private lateinit var dashUrl: String
    private lateinit var licenseUrl: String
    private var isPlayerInitialize = false
    private lateinit var audioBtn: ImageView
    private lateinit var subtitleBtn: ImageView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (BuildConfig.IS_SMART_TV) {
            setContentView(R.layout.layout_main_tv)
            playContent()

        } else {
            setContentView(R.layout.layout_main)
            urlEditText = findViewById(R.id.url_edit_text)
            licenseUrlEditText = findViewById(R.id.license_url_edit_text)
            playButton = findViewById(R.id.play_btn)
            playButton.setOnClickListener(this)
        }

        supportActionBar?.setDisplayShowHomeEnabled(true);
        supportActionBar?.setLogo(R.mipmap.kiora_logo);
        supportActionBar?.setDisplayUseLogoEnabled(true);

        audioBtn = findViewById(R.id.ic_audio)
        subtitleBtn = findViewById(R.id.ic_subtitle)
        audioBtn.setOnClickListener(this)
        subtitleBtn.setOnClickListener(this)

    }


    override fun onClick(v: View?) {

        when (v!!.id) {

            R.id.play_btn -> {

                if (isPlayerInitialize) {
                    player.release()
                    audioBtn.visibility = View.GONE
                    subtitleBtn.visibility = View.GONE
                }
                urlEditText.clearFocus()
                licenseUrlEditText.clearFocus()
                playContent()
            }
            R.id.ic_audio -> {
                showTracks(1)
            }
            R.id.ic_subtitle -> {
                showTracks(2)
            }
        }
    }

    private fun playContent() {

        if (BuildConfig.IS_SMART_TV) {

             //TODO Required for clear or widevine drm based content (For TV build playback)
            dashUrl = "Here pass drm url"

            //TODO Required for widevine drm based content but optional in clear content (For TV build playback)
            licenseUrl ="Here pass license url"
        } else {
            dashUrl = urlEditText.text.toString()
            licenseUrl = licenseUrlEditText.text.toString()
        }

        when {
            dashUrl.isEmpty() -> {
                urlEditText.error = "Field can't be empty"
            }

            else -> {
                initPlayer()
            }
        }
    }


    private fun initPlayer() {

        isPlayerInitialize = true

        val renderersFactory = DefaultRenderersFactory(applicationContext)
        bandwidthMeter = DefaultBandwidthMeter.Builder(applicationContext).build()
        selector = DefaultTrackSelector(this)

        player =
            SimpleExoPlayer.Builder(applicationContext, renderersFactory).setTrackSelector(selector)
                .setLoadControl(loadControl).setBandwidthMeter(bandwidthMeter).setLooper(mainLooper)
                .build()

        val playerView = findViewById<PlayerView>(R.id.player_view)
        playerView.player = player

        val dataSourceFactory = DefaultDataSourceFactory(
            applicationContext, bandwidthMeter,
            DefaultHttpDataSourceFactory(USER_AGENT, bandwidthMeter)
        )

        val drmCallback =
            HttpMediaDrmCallback(licenseUrl, DefaultHttpDataSourceFactory(USER_AGENT))
        drmSessionManager =
            DefaultDrmSessionManager.Builder().setMultiSession(true).build(drmCallback)
        val mediaSource = createDashSource(dashUrl, dataSourceFactory)

        player.prepare(mediaSource)
        player.playWhenReady = true


        this.player.addListener(object : Player.EventListener {
            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                when (playbackState) {
                    Player.STATE_IDLE -> {
                    }
                    Player.STATE_BUFFERING -> {
                    }
                    Player.STATE_READY -> {
                        audioBtn.visibility = View.VISIBLE
                        subtitleBtn.visibility = View.VISIBLE
                    }
                    Player.STATE_ENDED -> {
                    }
                }
            }
        })
    }

    private fun createDashSource(
        url: String,
        dataSourceFactory: DefaultDataSourceFactory
    ): DashMediaSource {
        return DashMediaSource.Factory(
            DefaultDashChunkSource.Factory(dataSourceFactory),
            dataSourceFactory
        ).setDrmSessionManager(drmSessionManager).createMediaSource(Uri.parse(url))
    }


    /**
     * Once you get the trackSelector object get the trackGroup array and after that pass the rendererIndex value
     * 0 -> Video
     * 1 -> Audio
     * 2 -> Text
     * */
    private fun showTracks(rendererIndex: Int) {

        val mappedTrackInfo: MappingTrackSelector.MappedTrackInfo =
            selector.currentMappedTrackInfo!!
        val trackType = mappedTrackInfo.getRendererType(rendererIndex)
        val trackGroupArray = mappedTrackInfo.getTrackGroups(rendererIndex)

        Log.e(
            "MAIN :: ", "------------------------------------------------------Track item "
                    + rendererIndex + "------------------------------------------------------"
        )
        Log.e("MAIN :: ", "track type: " + getTrackName(trackType))

        for (groupIndex in 0 until trackGroupArray.length) {
            for (trackIndex in 0 until trackGroupArray[groupIndex].length) {
                val trackName: String = trackGroupArray[groupIndex].getFormat(trackIndex).toString()
                val isTrackSupported = mappedTrackInfo.getTrackSupport(
                    rendererIndex,
                    groupIndex,
                    trackIndex
                ) == RendererCapabilities.FORMAT_HANDLED
                Log.e(
                    "MAIN :: ", "track item " + groupIndex + ": trackName: " + trackName
                            + ", isTrackSupported: " + isTrackSupported
                )
            }
        }
        val dialogBuilder = TrackSelectionDialogBuilder(
            this, title,
            selector, rendererIndex
        )
        dialogBuilder.setAllowAdaptiveSelections(true)
        dialogBuilder.build().show()
    }

    private fun getTrackName(trackType: Int): String? {
        when (trackType) {
            C.TRACK_TYPE_TEXT -> return "TRACK_TYPE_TEXT"
            C.TRACK_TYPE_AUDIO -> return "TRACK_TYPE_AUDIO"
            C.TRACK_TYPE_VIDEO -> return "TRACK_TYPE_VIDEO"
        }
        return ""
    }

    /**
     * Use this as per your custom UI if you want to change audio/subtitle according to your convenience.
     * */

    private fun overrideTracks() {

        // you'll get a track details from the TrackSelector
        val mappedTrackInfo: MappingTrackSelector.MappedTrackInfo =
            Assertions.checkNotNull(selector.currentMappedTrackInfo)
        val parameters: DefaultTrackSelector.Parameters = selector.parameters
        val builder = parameters.buildUpon()


        for (rendererIndex in 0 until mappedTrackInfo.rendererCount) {
            val trackType = mappedTrackInfo.getRendererType(rendererIndex)

            // Here you can choose trackType among TEXT, AUDIO, VIDEO, use multiple conditions to change  different tracks
            if (trackType == C.TRACK_TYPE_AUDIO) {
                builder.clearSelectionOverrides(rendererIndex)
                    .setRendererDisabled(rendererIndex, false)
                //{"data":0,"groupIndex":1,"length":1,"reason":2,"tracks":[0]}
                val groupIndex = 0
                val tracks = intArrayOf(0)
                val reason = 2
                val data = 0

                if (groupIndex == -1) {

                    //Clear track and override the previous one
                    builder.clearSelectionOverrides(rendererIndex)
                        .setRendererDisabled(rendererIndex, true)
                }
                val override =
                    DefaultTrackSelector.SelectionOverride(groupIndex, tracks, reason, data)
                builder.setSelectionOverride(
                    rendererIndex,
                    mappedTrackInfo.getTrackGroups(rendererIndex),
                    override
                )
            }
        }

        selector.setParameters(builder)
    }

    override fun onPause() {
        super.onPause()
        if (isPlayerInitialize) {
            player.playWhenReady = false
        }
    }

    override fun onResume() {
        super.onResume()
        if (isPlayerInitialize) {
            player.playWhenReady = true
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        isPlayerInitialize = false
    }
}

