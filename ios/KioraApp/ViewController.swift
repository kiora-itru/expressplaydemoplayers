//
//  ViewController.swift
//  KioraApp
//
//  Created by Prashanth Patil on 21/10/20.
//  Copyright © 2020 Intertrust. All rights reserved.
//

import UIKit
import AVKit

class ViewController: UIViewController {
    @IBOutlet weak var urlTextField: UITextField!
    @IBOutlet weak var licenseTextField: UITextField!
    @IBOutlet weak var certificateTextField: UITextField!
    @IBOutlet weak var kidTextField: UITextField!
    @IBOutlet weak var videoView: UIView!
    let playerViewController = AVPlayerViewController()
    var applicationCertificate = Data()
    var player: AVPlayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        overrideUserInterfaceStyle = .light
        // call the 'keyboardWillShow' function when the view controller receive the notification that a keyboard is going to be shown
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
         
        // call the 'keyboardWillHide' function when the view controlelr receive notification that keyboard is going to be hidden
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        
        //Code for left padding
        urlTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: urlTextField.frame.height))
        urlTextField.leftViewMode = .always
       
        //Code for left padding
        licenseTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: licenseTextField.frame.height))
        licenseTextField.leftViewMode = .always
        
        //Code for left padding
        certificateTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: certificateTextField.frame.height))
        certificateTextField.leftViewMode = .always
        
        //Code for left padding
        kidTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: kidTextField.frame.height))
        kidTextField.leftViewMode = .always

        //init toolbar
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(ViewController.doneButtonAction))
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        //setting toolbar as inputAccessoryView
        self.urlTextField.inputAccessoryView = toolbar
        self.certificateTextField.inputAccessoryView = toolbar
        self.licenseTextField.inputAccessoryView = toolbar
        self.kidTextField.inputAccessoryView = toolbar
    }
    @objc func doneButtonAction() {
       self.view.endEditing(true)
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
           // if keyboard size is not available for some reason, dont do anything
           return
        }
      // move the root view up by the distance of keyboard height
      self.view.frame.origin.y = 0 - keyboardSize.height
    }

    @objc func keyboardWillHide(notification: NSNotification) {
      // move back the root view origin to zero
      self.view.frame.origin.y = 0
    }

    
    @IBAction func playAction(_ sender: Any) {
        if (urlTextField.text != "" && licenseTextField.text != "" && certificateTextField.text != "" && kidTextField.text != "") {
            let url = URL(string:urlTextField.text!)
            let asset = AVURLAsset(url: url!)
            let playerItem = AVPlayerItem(asset: asset)
            player = AVPlayer(playerItem: playerItem)
            player.playImmediately(atRate: 1.0)
            asset.resourceLoader.setDelegate(self, queue: DispatchQueue.main)
            playerViewController.player = player
            playerViewController.view.frame.size.height = videoView.frame.size.height
            playerViewController.view.frame.size.width = videoView.frame.size.width
            self.videoView.addSubview(playerViewController.view)
            playerViewController.player?.play()
            
        }else if (urlTextField.text != "" && licenseTextField.text == "" && certificateTextField.text == "" && kidTextField.text == ""){
            let url = URL(string:urlTextField.text!)
            player = AVPlayer(url: url!)
            playerViewController.player = player
            playerViewController.view.frame.size.height = videoView.frame.size.height
            playerViewController.view.frame.size.width = videoView.frame.size.width
            self.videoView.addSubview(playerViewController.view)
            playerViewController.player?.play()
        }else{
            let alert = UIAlertController(title: "Alert", message: "Please enter valid inputs to play the kiora content", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
}

extension ViewController: AVAssetResourceLoaderDelegate {
    func resourceLoader(_ resourceLoader: AVAssetResourceLoader, shouldWaitForLoadingOfRequestedResource loadingRequest: AVAssetResourceLoadingRequest) -> Bool {
        guard
            let url = loadingRequest.request.url else {
            print("🔑", #function, "Unable to read the url/host data.")
            loadingRequest.finishLoading(with: NSError(domain: "com.icapps.error", code: -1, userInfo: nil))
            return false
          }
          print("🔑", #function, url)
        
        
        let urlString =  self.certificateTextField.text
        let certificateURL = urlString!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let certificateRequest = NSMutableURLRequest(url: NSURL(string: certificateURL!)! as URL,
                                                  cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: 10.0)
          certificateRequest.httpMethod = "GET"
          let c_session = URLSession(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: OperationQueue.main)
          let c_dataTask = c_session.dataTask(with: certificateRequest as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error as Any)
            } else {
                print(response as Any)
                if data?.count != nil {
                    self.applicationCertificate = data!
                }else{
              }
            }
          })
        c_dataTask.resume()
 
        guard let contentIdData = self.kidTextField.text?.data(using: String.Encoding.utf8),
            let spcData = try? loadingRequest.streamingContentKeyRequestData(forApp: applicationCertificate, contentIdentifier: contentIdData, options: nil),
              let dataRequest = loadingRequest.dataRequest else {
                  loadingRequest.finishLoading(with: NSError(domain: "com.icapps.error", code: -3, userInfo: nil))
                  print("Something went wrong. Couldn't fetch fairplay certificate.")
                  return false
          }
        let finalStr = spcData.base64EncodedString().replacingOccurrences(of: "+", with: "%2B")
        let postString = "spc=\(finalStr)&assetId=\(self.kidTextField.text ?? "")"
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]
        let VideoUrl = self.licenseTextField.text?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let request = NSMutableURLRequest(url: NSURL(string: VideoUrl!)! as URL,
                                                              cachePolicy: .useProtocolCachePolicy,
                                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postString.data(using: .utf8)
    
        let session = URLSession(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: OperationQueue.main)
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
        if (error != nil) {
            print(error as Any)
        } else {
            print(response as Any)
                if data?.count != nil {
                    var ckcData: Data? = nil
                    if let data = data, var responseString = String(data: data, encoding: .utf8) {
                      responseString = responseString.replacingOccurrences(of: "<ckc>", with: "").replacingOccurrences(of: "</ckc>", with: "")
                      ckcData = Data(base64Encoded: responseString)
                    }
                    if ckcData?.count != nil {
                      dataRequest.respond(with: ckcData!)
                      loadingRequest.finishLoading()
                    }else{
                      let alert = UIAlertController(title: "Alert", message: "Something went wrong. Couldn't fetch fairplay license.", preferredStyle: UIAlertController.Style.alert)
                      alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                      self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        })
        dataTask.resume()
        return true
    }
}
extension ViewController: URLSessionDelegate {
    public func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
       let urlCredential = URLCredential(trust: challenge.protectionSpace.serverTrust!)
       completionHandler(.useCredential, urlCredential)
    }
}
