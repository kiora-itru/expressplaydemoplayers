# README #

This README would normally document whatever steps are necessary to get your application up and running.

1. This app contains a basic implementation for the Clear and Encrypted (via Fairplay) HLS video playback.

2. Most of the code is written in the ViewController file. Inline comment are also added

3. For the Clear HLS content playback 
    - Enter the manifest URL in the UI and click the 'Play' button. (Remaining fields are optional)
    - By default it will support both Multi Track Audio and Subtitles

4. For the Encrypted (Fairplay) HLS content Playback
    - Enter all the fields (manifest URL, Fairplay License URL and Fairplay Certificate URL and ContentID) and click on the 'Play' button.
    - By default it will support both Multi Track Audio and Subtitles

For more details, please send an email to
kiora-support@intertrust.com
